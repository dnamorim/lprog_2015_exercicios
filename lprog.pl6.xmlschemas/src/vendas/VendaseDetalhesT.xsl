<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : VendaseDetalhesT.xsl
    Created on : May 16, 2012, 7:45 AM
    Author     : Administrator
    Description:
        Mostrar as Vendas, os detalhes e os totais
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:ns="http://xml.dei.isep.ipp.pt/schema/VendaseDetalhes">
    <xsl:output method="html"/>

    <xsl:template match="/">
        <html>
            <head>
                <title>Vendas e Detalhes</title>
                
                <script>                    
                    function hide()  {
                        var arr = document.getElementsByTagName("table");

                        for (var i = 0; i &lt; arr.length; i++)
                            arr[i].style.display = 'none';    
                    }
                    function show() {            
                        var arr = document.getElementsByTagName("table");

                        for (var i = 0; i &lt; arr.length; i++)
                            arr[i].style.display = '';    
                    }
                </script>
                
            </head>
            <body>              
                <FORM> 
                     <INPUT type="button" value="Mostrar Detalhes" name="btnMostrarDetalhes" onClick="show()"/> 
                     <INPUT type="button" value="Esconder Detalhes" name="btnEsconderDetalhes" onClick="hide()"/> 
                </FORM> 
                <hr/>
                <p><big>Listagem das Vendas</big></p><br/>
                <ol>
                <xsl:apply-templates select="//ns:venda"/>
                </ol>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="ns:venda">
        <li>
        Venda #<xsl:value-of select="ns:id"/> efectuada em <xsl:value-of select="ns:data"/> na loja  
        <xsl:value-of select="ns:loja"/> <br/>
        <table border="1" style="display:none">
        <tr><td align="center">#</td><td align="center">Produto</td><td align="center">Preco</td></tr>
            <xsl:variable name="idVendaVar" select="ns:id" />
            <xsl:apply-templates select="//ns:detalhe[@idVenda=$idVendaVar]"/>
            <tr> 
            <td align="center">Total</td>
            <td></td>
            <td align="right">€ <xsl:value-of select="format-number(sum(//ns:detalhe[@idVenda=$idVendaVar]/ns:preco), '#.00')"/> </td>
            </tr>            
        </table>        
        </li>
    </xsl:template>        
    <xsl:template match="ns:detalhe">        
        <tr> 
            <td align="center"> <xsl:value-of select="position()"/> </td>
            <td align="center"> <xsl:value-of select="ns:produto"/> </td>
            <td align="right"> <xsl:value-of select="ns:preco"/> </td>
        </tr>
    </xsl:template>        
</xsl:stylesheet>