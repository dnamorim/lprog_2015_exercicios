# lprog_2015_exercicios

Resolução dos exercícios da Unidade Curricular "Linguagens e Programação" (LPROG) do 2º Ano da [Licenciatura de Engenharia Informática](http://www.isep.ipp.pt/Course/Course/26) do [Instituto Superior de Engenharia do Porto](http://www.isep.ipp.pt) (LEI-ISEP).

© [Duarte Amorim](https://github.com/dnamorim), 2015