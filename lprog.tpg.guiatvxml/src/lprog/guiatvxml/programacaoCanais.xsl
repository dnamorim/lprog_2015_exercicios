<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:ns="http://xml.dei.isep.ipp.pt/schema/guiatv" exclude-result-prefixes="ns">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Programação Disponível dos Canais</title>
            </head>
            <body>
                <h1>Programação Disponível dos Canais</h1>
                
                <h2>Televisão</h2>
                <xsl:for-each select="//ns:canais/ns:tv/ns:canal">
                    <xsl:variable name="canalactual" select="@id" />
                    <table cellspacing="12">
                        <tr>
                            <td>
                                <img style="width:100px">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="ns:logo" />
                                    </xsl:attribute>
                                </img>
                            </td>
                            <td>
                                <table cellspacing="5">
                                    <tr>
                                        <td>
                                            <h3>
                                                <xsl:value-of select="ns:nome" /> (<xsl:value-of select="@id" />)</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <xsl:variable name="webpage" select="ns:pagina" />
                                            <b>Página Web: </b>&#160;
                                            <a>
                                                <xsl:attribute name="href">
                                                    <xsl:value-of select="$webpage" />
                                                </xsl:attribute>
                                                <xsl:value-of select="$webpage" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <xsl:for-each select="//ns:programacao/ns:tv/ns:programa[@canal=$canalactual]">
                        <xsl:apply-templates select="." />
                    </xsl:for-each>
                    <br/>
                </xsl:for-each>
                
                <br/>
                <hr/>
                <br/>
                <h2>Rádio</h2>
                <xsl:for-each select="//ns:canais/ns:radio/ns:canal">
                    <xsl:variable name="canalactual" select="@id" />
                    <table cellspacing="12">
                        <tr>
                            <td>
                                <img style="width:100px">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="ns:logo" />
                                    </xsl:attribute>
                                </img>
                            </td>
                            <td>
                                <table cellspacing="5">
                                    <tr>
                                        <td>
                                            <h3>
                                                <xsl:value-of select="ns:nome" /> (<xsl:value-of select="@id" />)</h3>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <xsl:variable name="webpage" select="ns:pagina" />
                                            <b>Página Web: </b>&#160;
                                            <a>
                                                <xsl:attribute name="href">
                                                    <xsl:value-of select="$webpage" />
                                                </xsl:attribute>
                                                <xsl:value-of select="$webpage" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br/>
                    <xsl:for-each select="//ns:programacao/ns:radio/ns:programa[@canal=$canalactual]">
                        <xsl:apply-templates select="." />
                    </xsl:for-each>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="ns:radio/ns:programa">
        <div id="programa_radio" style="padding-left:30px">
            <table cellspacing="18px">
                <tr>
                    <td>
                        <b>
                            <xsl:value-of select="ns:nome" />
                        </b><br/>
                        <div style="width:500px">
                        <xsl:value-of select="ns:sinopse" />
                        </div>
                    </td>
                    <td>
                        <b>Horário:&#160;</b>
                        <xsl:value-of select="ns:horainicio" /> - <xsl:value-of select="ns:horafim" />
                    </td>
                    <td>
                        <b>Categoria:&#160;</b>
                        <xsl:value-of select="ns:categoria" />
                    </td>
                </tr>
            </table>
        </div>
    </xsl:template>
    
    <xsl:template match="ns:tv/ns:programa">
        <div id="programa"  style="padding-left:30px">
            <table cellspacing="12px">
                <tr>
                    <td>
                        <img style="width:150px">
                            <xsl:attribute name="src">
                                <xsl:value-of select="ns:imagem" />
                            </xsl:attribute>
                        </img>
                    </td>
                    <td>
                        <h3>
                            <xsl:value-of select="ns:nome" />
                        </h3>
                        <table cellspacing="10px" style="text-align:left;">
                            <tr>
                                <th>Horário</th>
                                <th>Categoria</th>
                                <xsl:if test="count(ns:temporada)&gt;0">
                                    <th>Temporada</th>
                                    <th>Episódio</th>
                                </xsl:if>
                                <th>Formato</th>
                                <th>Audio Disponível</th>
                                <th>Legendas Disponíveis</th>
                            </tr>
                            <tr>
                                <td>
                                    <xsl:value-of select="ns:horainicio" /> - <xsl:value-of select="ns:horafim" />
                                </td>
                                <td> 
                                    <xsl:value-of select="ns:categoria" />
                                </td>
                                <xsl:if test="count(ns:temporada)&gt;0">
                                    <td>
                                        <xsl:value-of select="ns:temporada" />
                                    </td>
                                    <td>
                                        <xsl:value-of select="ns:episodio" />
                                    </td>
                                </xsl:if>
                                <td>
                                    <xsl:value-of select="ns:formato" />
                                </td>
                                <td>
                                    <xsl:for-each select="ns:pistas/ns:audio">
                                        <xsl:apply-templates>
                                            <xsl:value-of select="@lingua" />&#59;&#160; 
                                        </xsl:apply-templates>
                                    </xsl:for-each>
                                </td>
                                <td>
                                    <xsl:for-each select="ns:pistas/ns:legenda">
                                        <xsl:apply-templates>
                                            <xsl:value-of select="@lingua" />&#59;&#160; 
                                        </xsl:apply-templates>
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table> 
            <table cellspacing="10px" style="text-align:left; width:800px">
                <tr>
                    <th>Sinopse</th>
                </tr>
                <tr>
                    <td>
                        <xsl:value-of select="ns:sinopse" />
                    </td>
                </tr>
            </table>
            <br/>
            <br/>
        </div>
    </xsl:template>

</xsl:stylesheet>
