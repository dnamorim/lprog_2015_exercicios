<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
                xmlns:ns="http://xml.dei.isep.ipp.pt/schema/guiatv" exclude-result-prefixes="ns">
    <xsl:output method="xml" indent="yes"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:variable name="operadora" select="'vdf'" />
    <xsl:key name="keyTV" match="//ns:guia/ns:tv" use="@posicao"/>
    <xsl:key name="keyRadio" match="//ns:guia/ns:radio" use="@posicao"/>
    
    <xsl:template match="/">
        <operadora>
            <xsl:attribute name="code">
                <xsl:value-of select="$operadora" />
            </xsl:attribute>
            <xsl:apply-templates select="//ns:operador[@id=$operadora]" />
        </operadora>
    </xsl:template>

    <xsl:template match="ns:operador">
        <dados>
            <nome>
                <xsl:value-of select="ns:nome"/>
            </nome>
            <logotipo>
                <xsl:value-of select="ns:logo"/>
            </logotipo>
            <site>
                <xsl:value-of select="ns:pagina" />
            </site>
            <canais>
                <xsl:for-each select="//ns:guia[@operador=$operadora]/ns:tv[generate-id() = generate-id(key('keyTV', @posicao)[1])]">
                    <xsl:variable name="idTV" select="@programa"/>
                    <tv>
                        <xsl:attribute name="posicaoGrelha">
                            <xsl:value-of select="@posicao"/>
                        </xsl:attribute>
                        <xsl:apply-templates select="//ns:programacao/ns:tv/ns:programa[@id=$idTV]" />
                    </tv>
                </xsl:for-each>
                <xsl:for-each select="//ns:guia[@operador=$operadora]/ns:radio[generate-id() = generate-id(key('keyRadio', @posicao)[1])]">
                    <xsl:variable name="idRadio" select="@programa"/>
                    <radio>
                        <xsl:attribute name="posicaoGrelha">
                            <xsl:value-of select="@posicao"/>
                        </xsl:attribute>
                        <xsl:apply-templates select="//ns:programacao/ns:radio/ns:programa[@id=$idRadio]" />
                    </radio>
                </xsl:for-each>
            </canais>
            <videoclube>
                <xsl:attribute name="QtdTitulos">
                    <xsl:value-of select="count(//ns:guia[@operador=$operadora]/ns:videoclube)" />
                </xsl:attribute>
            </videoclube>
        </dados>
    </xsl:template>
    
    <xsl:template match="ns:tv/ns:programa">
        <xsl:variable name="channel" select="@canal" />
        <xsl:attribute name="cod">
            <xsl:value-of select="@canal" />
        </xsl:attribute>
        <nome>
            <xsl:value-of select="//ns:canal[@id=$channel]/ns:nome" />
        </nome>
    </xsl:template>
    
    <xsl:template match="ns:radio/ns:programa">
        <xsl:variable name="channel" select="@canal" />
        <xsl:attribute name="cod">
            <xsl:value-of select="@canal" />
        </xsl:attribute>
        <nome>
            <xsl:value-of select="//ns:canal[@id=$channel]/ns:nome" />
        </nome>
    </xsl:template>
</xsl:stylesheet>
