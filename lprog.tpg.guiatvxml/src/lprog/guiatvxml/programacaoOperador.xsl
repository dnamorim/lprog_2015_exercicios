<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : programacaoFiltrada.xsl
    Created on : 1 de Junho de 2015, 6:24
    Author     : dnamorim
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"  xmlns:ns="http://xml.dei.isep.ipp.pt/schema/guiatv" exclude-result-prefixes="ns xs">
    <xsl:output method="html"/>

    <xsl:variable name="operador" select="'meo'" />
    <xsl:template match="/">
        <html>
            <head>
                <title>Programação Filtrada</title>
            </head>
            <body>
                <h1>Programação Filtrada</h1>
                <div id="operadora">
                    <h2>
                        <xsl:value-of select="//ns:operadores/ns:operador[@id=$operador]/ns:nome" /> (<xsl:value-of select="$operador" />)
                    </h2>
                    <table cellspacing="10">
                        <tr>
                            <td></td>
                            <td>
                                <img style="height:100px">
                                    <xsl:attribute name="src">
                                        <xsl:value-of select="//ns:operadores/ns:operador[@id=$operador]/ns:logo" />
                                    </xsl:attribute>
                                </img>
                            </td>
                            <td>
                                <table cellspacing="5">
                                    <tr>
                                        <td>
                                            <b>Apoio ao Cliente</b>
                                        </td>
                                        <td>
                                            <a href="">
                                                <xsl:value-of select="//ns:operadores/ns:operador[@id=$operador]/ns:apoiocliente" />
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <b>Página da Operadora</b>
                                        </td>
                                        <td>
                                            <xsl:variable name="link_operadora" select="//ns:operadores/ns:operador[@id=$operador]/ns:pagina" />
                                            <a>
                                                <xsl:attribute name="href">
                                                    <xsl:value-of select="$link_operadora" />
                                                </xsl:attribute>
                                                <xsl:value-of select="$link_operadora" />
                                            </a>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                
                <h2>Televisão</h2>
                <xsl:apply-templates select="//ns:guia[@operador=$operador]/ns:tv">
                    <xsl:sort select="@emissao" />
                </xsl:apply-templates>
                <br/>
                <hr/>
                <br/>
                <h2></h2>
                <xsl:apply-templates select="//ns:guia[@operador=$operador]/ns:radio">
                    <xsl:sort select="@emissao" />
                </xsl:apply-templates>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="ns:guia/ns:tv">
        <xsl:variable name="programa" select="@programa" />
        <p style="font-size:18px">
            <b>Emissão:</b>&#160;
            <xsl:value-of select="@emissao" />&#160;&#160;
            <b>Horário:&#160;</b>
            <xsl:value-of select="//ns:programacao/ns:tv/ns:programa[@id=$programa]/ns:horainicio" /> - <xsl:value-of select="//ns:programacao/ns:tv/ns:programa[@id=$programa]/ns:horafim" />
            &#160;&#160;
            <b>Posição Canal: </b>
            <xsl:value-of select="@posicao" />
        </p>
        <xsl:apply-templates select="//ns:programacao/ns:tv/ns:programa[@id=$programa]" />
    </xsl:template>
    
    <xsl:template match="ns:guia/ns:radio">
        <xsl:variable name="programa" select="@programa" />
        <p style="font-size:18px">
            <b>Emissão:</b>&#160;
            <xsl:value-of select="@emissao" />&#160;&#160;
            <b>Horário:&#160;</b>
            <xsl:value-of select="//ns:programacao/ns:radio/ns:programa[@id=$programa]/ns:horainicio" /> - <xsl:value-of select="//ns:programacao/ns:radio/ns:programa[@id=$programa]/ns:horafim" />
            &#160;&#160;
            <b>Posição Canal: </b>
            <xsl:value-of select="@posicao" />
        </p>
        <xsl:apply-templates select="//ns:programacao/ns:radio/ns:programa[@id=$programa]" />
    </xsl:template>
    
    <xsl:template match="ns:programacao/ns:radio/ns:programa">
        <xsl:variable name="canal" select="@canal" />
        <table cellspacing="10px" style="padding-left:25px">
            <tr>
                <td>
                    <img width="115px">
                        <xsl:attribute name="src">
                            <xsl:value-of select="//ns:canais/ns:radio/ns:canal[@id=$canal]/ns:logo" />
                        </xsl:attribute>
                    </img>
                </td>
                <td>
                    <table style="text-align:left" cellspacing="10px">
                        <tr>
                            <td>
                                <th>Canal: </th>
                            </td>
                            <td>
                                <xsl:value-of select="//ns:canais/ns:radio/ns:canal[@id=$canal]/ns:nome" /> (<xsl:value-of select="$canal" />)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <th>Nome: </th>
                            </td>
                            <td><xsl:value-of select="ns:nome" /></td>
                        </tr>
                        <tr>
                            <td>
                                <th>Categoria: </th>
                            </td>
                            <td><xsl:value-of select="ns:categoria" /></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template match="ns:programacao/ns:tv/ns:programa">
        <xsl:variable name="canal" select="@canal" />
        <table cellspacing="10px" style="padding-left:25px">
            <tr>
                <td>
                    <img width="115px">
                        <xsl:attribute name="src">
                            <xsl:value-of select="ns:imagem" />
                        </xsl:attribute>
                    </img>
                </td>
                <td>
                    <table style="text-align:left" cellspacing="10px">
                        <tr>
                            <td>
                                <th>Canal: </th>
                            </td>
                            <td>
                                <xsl:value-of select="//ns:canais/ns:tv/ns:canal[@id=$canal]/ns:nome" /> (<xsl:value-of select="$canal" />)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <th>Nome: </th>
                            </td>
                            <td><xsl:value-of select="ns:nome" /></td>
                        </tr>
                        <tr>
                            <td>
                                <th>Categoria: </th>
                            </td>
                            <td><xsl:value-of select="ns:categoria" /></td>
                        </tr>
                        <xsl:if test="count(ns:temporada) &gt;0" >
                            <tr>
                                <td>
                                    <th>Temporada</th>
                                </td>
                                <td>T<xsl:value-of select="ns:temporada" /> - E<xsl:value-of select="ns:episodio" /></td>
                            </tr>
                        </xsl:if> 
                    </table>
                </td>
            </tr>
        </table>
    </xsl:template>

</xsl:stylesheet>
