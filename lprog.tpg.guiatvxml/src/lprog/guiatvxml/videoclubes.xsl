<?xml version="1.0" encoding="UTF-8"?>

<!--
    Document   : videoclubes.xsl
    Created on : 31 de Maio de 2015, 20:49
    Author     : dnamorim
    Description:
        Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:ns="http://xml.dei.isep.ipp.pt/schema/guiatv" exclude-result-prefixes="ns">
    <xsl:output method="html"/>

    <!-- TODO customize transformation rules 
         syntax recommendation http://www.w3.org/TR/xslt 
    -->
    <xsl:template match="/">
        <html>
            <head>
                <title>Videoclubes das Operadoras</title>
            </head>
            <body>
                <h1>Videoclubes das Operadoras</h1>
                <xsl:for-each select="//ns:guia">
                    <xsl:variable name="idOperador" select="@operador" />
                    <xsl:variable name="count_vdc" select="count(ns:videoclube)" />
                    
                    <xsl:if test="$count_vdc &gt; 0">
                        <div id="operadora">
                            <h2>
                                <xsl:value-of select="//ns:operadores/ns:operador[@id=$idOperador]/ns:nome" />
                            </h2>
                            <table cellspacing="10">
                                <tr>
                                    <td></td>
                                    <td>
                                        <img style="height:100px">
                                            <xsl:attribute name="src">
                                                <xsl:value-of select="//ns:operadores/ns:operador[@id=$idOperador]/ns:logo" />
                                            </xsl:attribute>
                                        </img>
                                    </td>
                                    <td>
                                        <table cellspacing="5">
                                            <tr>
                                                <td>
                                                    <b>Apoio ao Cliente</b>
                                                </td>
                                                <td>
                                                    <a href="">
                                                        <xsl:value-of select="//ns:operadores/ns:operador[@id=$idOperador]/ns:apoiocliente" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Página da Operadora</b>
                                                </td>
                                                <td>
                                                    <xsl:variable name="link_operadora" select="//ns:operadores/ns:operador[@id=$idOperador]/ns:pagina" />
                                                    <a>
                                                        <xsl:attribute name="href">
                                                            <xsl:value-of select="$link_operadora" />
                                                        </xsl:attribute>
                                                        <xsl:value-of select="$link_operadora" />
                                                    </a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <b>Títulos Videoclube Disponíveis</b>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="$count_vdc" /> títulos
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br/>
                        <div id="titulosVDC">
                            <h2>Títulos do Videoclube</h2>
                            <xsl:apply-templates select="ns:videoclube" />
                        </div>
                        <br/><hr/><br/>
                    </xsl:if>
                </xsl:for-each>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="ns:guia/ns:videoclube">
        <xsl:variable name="idVideoclube" select="@id" />
        <xsl:variable name="preco" select="@preco" />
        <p>
            <xsl:apply-templates select="//ns:videoclube/ns:titulovdc[@id=$idVideoclube]">
                <xsl:with-param name="preco_vdc" select="$preco"/>
            </xsl:apply-templates>
        </p>
    </xsl:template>
    
    <xsl:template match="ns:titulovdc">
        <xsl:param name="preco_vdc"/>
        <table cellspacing="10">
            <tr>
                <td>
                    <img style="width:150px">
                        <xsl:attribute name="src">
                            <xsl:value-of select="ns:imagem" />
                        </xsl:attribute>
                    </img>
                </td>
                <td>
                    <table cellspacing="6">
                        <tr>
                            <td>
                                <h2>
                                    <xsl:value-of select="ns:titulo" /> (<xsl:value-of select="ns:ano" />)</h2>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img src="sources/imdb.png" style="width:60px" />
                            &#160;&#160;
                                <xsl:variable name="imdb" select="ns:imdb" />
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:value-of select="$imdb" />
                                    </xsl:attribute>
                                    <xsl:value-of select="$imdb" />
                                </a>
                             </td>
                             <td>
                                <b>Pontuação:</b>&#160;
                                <xsl:value-of select="ns:pontuacao" />/10
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Realizador:</b>&#160;
                                <xsl:value-of select="ns:realizador" />
                            </td>
                            <td>
                                <b>Actores Principais:</b>&#160;
                                <xsl:for-each select="ns:actores/ns:actor">
                                    <xsl:value-of select="." />&#59;&#160; 
                                </xsl:for-each>
                                
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Categoria: </b>&#160;
                                <xsl:value-of select="ns:categoria" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Sinopse</b>
                            </td>
                            <td>
                                <b>Preço</b>&#160;
                                <xsl:value-of select="$preco_vdc" /> €
                            </td>
                        </tr>
                        <tr>
                            <td style="width:600px">
                               <xsl:value-of select="ns:sinopse" /> 
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </xsl:template>
</xsl:stylesheet>
