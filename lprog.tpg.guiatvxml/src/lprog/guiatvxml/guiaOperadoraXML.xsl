<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0"
    xmlns:ns="http://xml.dei.isep.ipp.pt/schema/guiatv" exclude-result-prefixes="ns">
    <xsl:output method="xml" indent="yes"/>
    
    <xsl:template match="/">
        <!-- Operadora a Filtrar -->
        <xsl:variable name="operador" select="'vdf'" />
        
        <programacao>
            <xsl:apply-templates select="//ns:guia[@operador=$operador]" />
        </programacao>
        
    </xsl:template>
    
    <xsl:template match="ns:guia">
        <tv>
            <xsl:attribute name="count">
                <xsl:value-of select="count(ns:tv)" />
            </xsl:attribute>
            <xsl:for-each select="ns:tv">
                <xsl:sort select="@emissao"/>
                <xsl:apply-templates select="." />
            </xsl:for-each>
        </tv>
        <radio>
            <xsl:attribute name="count">
                <xsl:value-of select="count(ns:radio)" />
            </xsl:attribute>
            <xsl:for-each select="ns:radio">
                <xsl:sort select="@emissao"/>
                <xsl:apply-templates select="." />
            </xsl:for-each>
        </radio>
        <videoclube>
            <xsl:attribute name="count">
                <xsl:value-of select="count(ns:videoclube)" />
            </xsl:attribute>
            <xsl:for-each select="ns:videoclube">
                <xsl:apply-templates select="." />
            </xsl:for-each>
        </videoclube>
    </xsl:template>
    
    <xsl:template match="ns:guia/ns:videoclube">
        <titulo>
            <xsl:variable name="idVDC" select="@id" />
            <xsl:attribute name="preco">
                <xsl:value-of select="@preco" />
            </xsl:attribute>
            <xsl:apply-templates select="//ns:videoclube/ns:titulovdc[@id=$idVDC]" />
        </titulo>
    </xsl:template>
    
    <xsl:template match="ns:videoclube/ns:titulovdc">
        <xsl:value-of select="ns:titulo" /> (<xsl:value-of select="ns:ano" />)
    </xsl:template>
    
    <xsl:template match="ns:guia/ns:tv">
        <programa>
            <xsl:variable name="idPrograma" select="@programa" />
            <xsl:attribute name="posicao">
                <xsl:value-of select="@posicao" />
            </xsl:attribute>
            <xsl:attribute name="data">
                <xsl:value-of select="@emissao" />
            </xsl:attribute>
            <xsl:apply-templates select="//ns:programacao/ns:tv/ns:programa[@id=$idPrograma]" />
        </programa>
    </xsl:template>
    
    <xsl:template match="ns:programacao/ns:tv/ns:programa">
        <xsl:attribute name="canal">
            <xsl:value-of select="@canal"/>
        </xsl:attribute>
        <xsl:attribute name="horario">
            <xsl:value-of select="ns:horainicio" /> - <xsl:value-of select="ns:horafim" />
        </xsl:attribute>
        <titulo><xsl:value-of select="ns:nome" /></titulo>
        <categoria><xsl:value-of select="ns:categoria" /></categoria>
        <resumo><xsl:value-of select="ns:sinopse" /></resumo>
        <xsl:if test="count(ns:temporada) &gt; 0"> 
            <episodio>S<xsl:value-of select="ns:temporada" />E<xsl:value-of select="ns:episodio" /></episodio>
        </xsl:if>
        <thumbnail><xsl:value-of select="ns:imagem" /></thumbnail>
        
    </xsl:template>


  <xsl:template match="ns:guia/ns:radio">
        <programa>
            <xsl:variable name="idPrograma" select="@programa" />
            <xsl:attribute name="posicao">
                <xsl:value-of select="@posicao" />
            </xsl:attribute>
            <xsl:attribute name="data">
                <xsl:value-of select="@emissao" />
            </xsl:attribute>
            <xsl:apply-templates select="//ns:programacao/ns:radio/ns:programa[@id=$idPrograma]" />
        </programa>
    </xsl:template>
    
    <xsl:template match="ns:programacao/ns:radio/ns:programa">
        <xsl:attribute name="canal">
            <xsl:value-of select="@canal"/>
        </xsl:attribute>
        <xsl:attribute name="horario">
            <xsl:value-of select="ns:horainicio" /> - <xsl:value-of select="ns:horafim" />
        </xsl:attribute>
        <titulo><xsl:value-of select="ns:nome" /></titulo>
        <categoria><xsl:value-of select="ns:categoria" /></categoria>
        <resumo><xsl:value-of select="ns:sinopse" /></resumo>
    </xsl:template>
</xsl:stylesheet>
