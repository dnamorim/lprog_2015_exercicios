<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:ns="http://www.dei.isep.ipp.pt/lprog" exclude-result-prefixes="ns">
    <xsl:output method="html"/>
   
    <xsl:template match="/">
        <html>
            <head>
                <title>Relatório de Linguagens de Programação</title>
            </head>
            <body bgcolor="#FFFFFF" text="#8E2323" link="#808080">
                <div align="center">
                    <font face="calibri">
                        <img src="http://bit.ly/T1SQfq"/>
                        <br></br>
                        <br></br>
                        
                        <h1 id="heading1">
                            <font size="15">
                                <b>Relatório de Linguagens de Programação</b>
                            </font>
                        </h1>
                            
                        <br></br>
                        <br></br>
                        <font size="5" color="#343434">
                            <font color="#8E2323">Tema </font>
                            <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:tema"/>
                        </font>
                       
                        <br></br>
                        <br></br>
                        
                        <font size="5" color="#343434">
                            <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:disciplina/ns:tema"/>
                            <p></p>
                            <font size="5" color="#343434">Instituto Superior de Engenharia do Porto - Licenciatura em Engenharia Informática</font>
                            <br></br>
                            <font size="5" color="#343434">
                                <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:disciplina/ns:designação"/>: <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:disciplina/ns:anoCurricular"/>º Ano Curricular</font>
                            <p></p>
                            <font size="5" color="#343434">Ano Lectivo 2014/2015</font>
                            <p></p>
                            <font size="5" color="#343434">
                                <font color="#8E2323">Turma: </font>
                                <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:turmaPL"/>
                            </font>
                            <br></br>
                            <font size="5" color="#343434">
                                <font color="#8E2323">Professor: </font>José António Tavares - <font color="#8E2323">Sigla: </font>
                                <xsl:value-of select="ns:relatório/ns:páginaRosto/ns:profPL"/>
                            </font>
                            <p></p>
                            <font size="5" color="#343434">
                                <font color="#8E2323">Autores:</font>
                                <br></br>
                                <xsl:for-each select="ns:relatório/ns:páginaRosto/ns:autor">
                                    <xsl:value-of select="ns:nome"/> - <xsl:value-of select="ns:mail"/>
                                    <br></br>
                                </xsl:for-each>                          
                            </font>
                            <p></p>
                        
                            <hr color="#808080"></hr>
                            <p></p>
                            <font size="7" color="#8E2323">
                                <b>Índice</b>
                            </font>
                            <p></p>
                        
                            <font size="5">
                                <a href="#heading2">Introdução</a>
                                <br></br>
                                <a href="#heading3">Análise</a>
                                <br></br>
                                <a href="#heading4">Linguagem</a>
                                <br></br>
                                <a href="#heading5">Transformações</a>
                                <br></br>
                                <a href="#heading6">Conclusão</a>
                                <br></br>
                                <a href="#heading7">Referências bibliográficas</a>
                                <br></br>
                                <a href="#heading8">Anexos</a>
                                <br></br>                        
                            </font>
                            <p></p>
                        
                            <hr color="#808080"></hr>
                        
                            <h1 id="heading2">
                                <font size="7" color="#8E2323" >
                                    <b>Introdução</b>
                                </font>
                            </h1>
                            <font size="5" color="#343434">
                                <p align="justify">
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:introdução/ns:bloco/ns:parágrafo[1]"/>
                                    <br/>
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:introdução/ns:bloco/ns:parágrafo[2]"/>
                                    <br/>
                            
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:introdução/ns:bloco/ns:listaItems/ns:item">
                                        - <xsl:value-of select="text()"/>
                                        <br/>
                                    </xsl:for-each> 
                                    <br></br>
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:introdução/ns:bloco/ns:parágrafo[3]"/>
                                    <br/>
                                </p>
                            </font>
                        
                            <hr color="#808080"></hr>
                        
                            <h1 id="heading3">
                                <font size="7" color="#8E2323" >
                                    <b>Análise</b>
                                </font>
                            </h1>
                            <font size="5" color="#343434">
                                <p align="justify">
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:análise/ns:bloco/ns:parágrafo[1] " />
                                    <br/>
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:análise/ns:bloco/ns:parágrafo[2]" />
                                    <br/>
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:análise/ns:bloco/ns:parágrafo[3]" />
                                    <br/>
                                    <br></br>
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:introdução/ns:bloco/ns:listaItems/ns:item">
                                        - <xsl:value-of select="text()"/>
                                        <br/>
                                    </xsl:for-each> 
                                    <br></br>
                                </p>
                            </font>
                        
                            <hr color="#808080"></hr>
                        
                            <h1 id="heading4">
                                <font size="7" color="#8E2323" >
                                    <b>Linguagem</b>
                                </font>
                            </h1>
                            <font size="5" color="#343434">
                                <p align="justify">
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:parágrafo[1]"/>
                                    <br></br>
                                    <xsl:apply-templates select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:figura[1]" />
                                    <br/>
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:parágrafo[2]"/>
                                    <br></br>
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:listaItems/ns:item">
                                        - <xsl:value-of select="text()"/>
                                        <br/>
                                    </xsl:for-each> 
                                    <br/>
                                    <xsl:apply-templates select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:figura[2]" />
                                    <xsl:apply-templates select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:figura[3]" />
                                    <xsl:apply-templates select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:figura[4]" />
                                    <xsl:apply-templates select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:figura[5]" />
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:linguagem/ns:bloco/ns:parágrafo[3]"/>
                                    <br/>
                                </p>
                                <br></br>
                            </font>
                        
                            <hr color="#808080"></hr>
                        
                            <h1 id="heading5">
                                <font size="7" color="#8E2323" >
                                    <b>Transformações</b>
                                </font>
                            </h1>
                            <font size="5" color="#343434">
                                <p align="justify">
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:transformações/ns:bloco/ns:parágrafo[1]"/>
                                    <br></br>
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:transformações/ns:bloco/ns:parágrafo[2]"/>
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:secções/ns:transformações/ns:bloco/ns:listaItems/ns:item">
                                        - <xsl:value-of select="text()"/>
                                        <br/>
                                    </xsl:for-each> 
                                    <br></br>
                                    <xsl:value-of select="ns:relatório/ns:corpo/ns:secções/ns:transformações/ns:bloco/ns:parágrafo[3]"/>
                                    <br></br>
                                </p>
                                <br></br>
                            </font>
                        
                            <hr color="#808080"></hr>
                        
                            <h1 id="heading6">
                                <font size="7" color="#8E2323" >
                                    <b>Conclusão</b>
                                </font>
                            </h1>
                            <font size="5" color="#343434">
                                <p align="justify">
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:conclusão/ns:bloco/ns:parágrafo">
                                        <xsl:value-of select="text()"/>
                                        <br></br>
                                    </xsl:for-each>
                                </p>
                                <br></br>
                            </font>
                            <hr color="#808080"></hr>
                            <p></p>
                        
                            <h1 id="heading7">
                                <font size="7" color="#8E2323" >
                                    <b>Referências Bibliográficas</b>
                                </font>
                            </h1>
                            <font size="3" color="#343434">
                                <p align="center">
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:referências/ns:refBibliográfica">
                                        <p><xsl:value-of select="ns:título" />; Autor <xsl:value-of select="ns:autor" /> (<xsl:value-of select="ns:dataPublicação" />)
                                        </p>
                                    </xsl:for-each>
                                    <xsl:for-each select="ns:relatório/ns:corpo/ns:referências/ns:refWeb">
                                        <p><xsl:value-of select="ns:descrição" /> - URL <a>
                                            <xsl:attribute name="href">
                                                <xsl:value-of select="ns:URL" />
                                            </xsl:attribute>
                                            <xsl:value-of select="ns:URL" /> 
                                        </a>(<xsl:value-of select="ns:consultadoEm" />)</p>
                                    </xsl:for-each>
                                </p>
                                <br></br>
                            </font>
                        
                            <hr color="#808080"></hr>
                            <p></p>
                        
                            <h1 id="heading8">
                                <font size="7" color="#8E2323" >
                                    <b>Anexos</b>
                                </font>
                            </h1>
                            <font size="3" color="#343434">
                                <p align="center">
                                    <xsl:for-each select="ns:relatório/ns:anexos/ns:bloco">
                                        <p>
                                            <xsl:value-of select="ns:parágrafo" />
                                            <xsl:apply-templates select="ns:figura" />
                                            <br/>
                                        </p>
                                   </xsl:for-each>
                                </p>
                                <br></br>
                            </font>
                        </font>
                    </font>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="ns:figura">
        <div style="text-size:12px">
        <img style="width:500px">
            <xsl:attribute name="src">
                <xsl:value-of select="@src" />
            </xsl:attribute>
        </img>
        <br/><br/>
        
            <xsl:value-of select="@descrição"/>
        </div>
        
    </xsl:template>
</xsl:stylesheet>

