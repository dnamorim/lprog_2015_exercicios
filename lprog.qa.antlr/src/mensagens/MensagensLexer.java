// Generated from Mensagens.g4 by ANTLR 4.5
package mensagens;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MensagensLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NEWLINE=1, HASH=2, TELEFONE=3, DATA=4, HORA=5, CUSTO=6, MENSAGEM=7, WS=8;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] ruleNames = {
		"NEWLINE", "HASH", "TELEFONE", "DATA", "HORA", "CUSTO", "MENSAGEM", "WS"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'#'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NEWLINE", "HASH", "TELEFONE", "DATA", "HORA", "CUSTO", "MENSAGEM", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}


	public MensagensLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "Mensagens.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\ng\b\1\4\2\t\2\4"+
		"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\3\3\3"+
		"\3\4\3\4\3\4\5\4\33\n\4\3\4\3\4\3\4\5\4 \n\4\5\4\"\n\4\3\4\3\4\3\4\3\4"+
		"\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3\5\5\5\64\n\5\3\5\3\5\3"+
		"\5\3\5\3\5\5\5;\n\5\3\5\3\5\3\5\3\5\3\5\3\5\3\6\3\6\3\6\3\6\5\6G\n\6\3"+
		"\6\3\6\3\6\3\6\3\7\3\7\3\7\7\7P\n\7\f\7\16\7S\13\7\5\7U\n\7\3\7\3\7\3"+
		"\7\3\7\3\7\3\b\3\b\6\b^\n\b\r\b\16\b_\3\b\3\b\3\t\3\t\3\t\3\t\2\2\n\3"+
		"\3\5\4\7\5\t\6\13\7\r\b\17\t\21\n\3\2\20\3\2\f\f\3\2\62\62\3\2\62;\3\2"+
		"\63;\3\2\63\64\3\2\65\65\3\2\62\63\3\2\63\63\3\2\62\64\3\2\64\64\3\2\62"+
		"\65\3\2\62\67\5\2\"\"C\\c|\5\2\13\13\17\17\"\"p\2\3\3\2\2\2\2\5\3\2\2"+
		"\2\2\7\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21"+
		"\3\2\2\2\3\23\3\2\2\2\5\25\3\2\2\2\7!\3\2\2\2\t\63\3\2\2\2\13F\3\2\2\2"+
		"\rT\3\2\2\2\17[\3\2\2\2\21c\3\2\2\2\23\24\t\2\2\2\24\4\3\2\2\2\25\26\7"+
		"%\2\2\26\6\3\2\2\2\27\33\7-\2\2\30\31\t\3\2\2\31\33\t\3\2\2\32\27\3\2"+
		"\2\2\32\30\3\2\2\2\33\34\3\2\2\2\34\35\t\4\2\2\35\37\t\4\2\2\36 \t\4\2"+
		"\2\37\36\3\2\2\2\37 \3\2\2\2 \"\3\2\2\2!\32\3\2\2\2!\"\3\2\2\2\"#\3\2"+
		"\2\2#$\t\4\2\2$%\t\4\2\2%&\t\4\2\2&\'\t\4\2\2\'(\t\4\2\2()\t\4\2\2)*\t"+
		"\4\2\2*+\t\4\2\2+,\t\4\2\2,\b\3\2\2\2-.\t\3\2\2.\64\t\5\2\2/\60\t\6\2"+
		"\2\60\64\t\4\2\2\61\62\t\7\2\2\62\64\t\b\2\2\63-\3\2\2\2\63/\3\2\2\2\63"+
		"\61\3\2\2\2\64\65\3\2\2\2\65:\7\61\2\2\66\67\t\3\2\2\67;\t\5\2\289\t\t"+
		"\2\29;\t\n\2\2:\66\3\2\2\2:8\3\2\2\2;<\3\2\2\2<=\7\61\2\2=>\t\4\2\2>?"+
		"\t\4\2\2?@\t\4\2\2@A\t\4\2\2A\n\3\2\2\2BC\t\b\2\2CG\t\4\2\2DE\t\13\2\2"+
		"EG\t\f\2\2FB\3\2\2\2FD\3\2\2\2GH\3\2\2\2HI\7<\2\2IJ\t\r\2\2JK\t\4\2\2"+
		"K\f\3\2\2\2LU\t\3\2\2MQ\t\5\2\2NP\t\4\2\2ON\3\2\2\2PS\3\2\2\2QO\3\2\2"+
		"\2QR\3\2\2\2RU\3\2\2\2SQ\3\2\2\2TL\3\2\2\2TM\3\2\2\2UV\3\2\2\2VW\7\60"+
		"\2\2WX\t\4\2\2XY\t\4\2\2YZ\t\4\2\2Z\16\3\2\2\2[]\7*\2\2\\^\t\16\2\2]\\"+
		"\3\2\2\2^_\3\2\2\2_]\3\2\2\2_`\3\2\2\2`a\3\2\2\2ab\7+\2\2b\20\3\2\2\2"+
		"cd\t\17\2\2de\3\2\2\2ef\b\t\2\2f\22\3\2\2\2\f\2\32\37!\63:FQT_\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}