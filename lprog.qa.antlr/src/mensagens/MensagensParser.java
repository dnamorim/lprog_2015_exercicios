// Generated from Mensagens.g4 by ANTLR 4.5
package mensagens;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MensagensParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.5", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		NEWLINE=1, HASH=2, TELEFONE=3, DATA=4, HORA=5, CUSTO=6, MENSAGEM=7, WS=8;
	public static final int
		RULE_log = 0, RULE_sms = 1, RULE_destinos = 2, RULE_destino = 3;
	public static final String[] ruleNames = {
		"log", "sms", "destinos", "destino"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, "'#'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "NEWLINE", "HASH", "TELEFONE", "DATA", "HORA", "CUSTO", "MENSAGEM", 
		"WS"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "Mensagens.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public MensagensParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class LogContext extends ParserRuleContext {
		public SmsContext sms() {
			return getRuleContext(SmsContext.class,0);
		}
		public TerminalNode NEWLINE() { return getToken(MensagensParser.NEWLINE, 0); }
		public LogContext log() {
			return getRuleContext(LogContext.class,0);
		}
		public LogContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_log; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MensagensVisitor ) return ((MensagensVisitor<? extends T>)visitor).visitLog(this);
			else return visitor.visitChildren(this);
		}
	}

	public final LogContext log() throws RecognitionException {
		LogContext _localctx = new LogContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_log);
		try {
			setState(13);
			switch ( getInterpreter().adaptivePredict(_input,0,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(8);
				sms();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(9);
				sms();
				setState(10);
				match(NEWLINE);
				setState(11);
				log();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class SmsContext extends ParserRuleContext {
		public Token MENSAGEM;
		public TerminalNode TELEFONE() { return getToken(MensagensParser.TELEFONE, 0); }
		public TerminalNode DATA() { return getToken(MensagensParser.DATA, 0); }
		public TerminalNode HORA() { return getToken(MensagensParser.HORA, 0); }
		public DestinosContext destinos() {
			return getRuleContext(DestinosContext.class,0);
		}
		public TerminalNode MENSAGEM() { return getToken(MensagensParser.MENSAGEM, 0); }
		public SmsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_sms; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MensagensVisitor ) return ((MensagensVisitor<? extends T>)visitor).visitSms(this);
			else return visitor.visitChildren(this);
		}
	}

	public final SmsContext sms() throws RecognitionException {
		SmsContext _localctx = new SmsContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_sms);
		try {
			setState(28);
			switch ( getInterpreter().adaptivePredict(_input,1,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(15);
				match(TELEFONE);
				setState(16);
				match(DATA);
				setState(17);
				match(HORA);
				setState(18);
				destinos();
				setState(19);
				((SmsContext)_localctx).MENSAGEM = match(MENSAGEM);
				 if(((SmsContext)_localctx).MENSAGEM.getText().length() > 160) { notifyErrorListeners("Limite SMS excedido.\n"); }
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(22);
				match(TELEFONE);
				setState(23);
				match(DATA);
				setState(24);
				destinos();
				setState(25);
				((SmsContext)_localctx).MENSAGEM = match(MENSAGEM);
				 if(((SmsContext)_localctx).MENSAGEM.getText().length() > 160) { notifyErrorListeners("Limite SMS excedido.\n"); }
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DestinosContext extends ParserRuleContext {
		public DestinoContext destino() {
			return getRuleContext(DestinoContext.class,0);
		}
		public DestinosContext destinos() {
			return getRuleContext(DestinosContext.class,0);
		}
		public DestinosContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destinos; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MensagensVisitor ) return ((MensagensVisitor<? extends T>)visitor).visitDestinos(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DestinosContext destinos() throws RecognitionException {
		DestinosContext _localctx = new DestinosContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_destinos);
		try {
			setState(34);
			switch ( getInterpreter().adaptivePredict(_input,2,_ctx) ) {
			case 1:
				enterOuterAlt(_localctx, 1);
				{
				setState(30);
				destino();
				setState(31);
				destinos();
				}
				break;
			case 2:
				enterOuterAlt(_localctx, 2);
				{
				setState(33);
				destino();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class DestinoContext extends ParserRuleContext {
		public TerminalNode TELEFONE() { return getToken(MensagensParser.TELEFONE, 0); }
		public List<TerminalNode> HASH() { return getTokens(MensagensParser.HASH); }
		public TerminalNode HASH(int i) {
			return getToken(MensagensParser.HASH, i);
		}
		public TerminalNode CUSTO() { return getToken(MensagensParser.CUSTO, 0); }
		public DestinoContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_destino; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof MensagensVisitor ) return ((MensagensVisitor<? extends T>)visitor).visitDestino(this);
			else return visitor.visitChildren(this);
		}
	}

	public final DestinoContext destino() throws RecognitionException {
		DestinoContext _localctx = new DestinoContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_destino);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(36);
			match(TELEFONE);
			setState(37);
			match(HASH);
			setState(38);
			match(CUSTO);
			setState(39);
			match(HASH);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\n,\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\3\2\3\2\3\2\3\2\3\2\5\2\20\n\2\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3\37\n\3\3\4\3\4\3\4\3\4\5\4%\n\4\3"+
		"\5\3\5\3\5\3\5\3\5\3\5\2\2\6\2\4\6\b\2\2*\2\17\3\2\2\2\4\36\3\2\2\2\6"+
		"$\3\2\2\2\b&\3\2\2\2\n\20\5\4\3\2\13\f\5\4\3\2\f\r\7\3\2\2\r\16\5\2\2"+
		"\2\16\20\3\2\2\2\17\n\3\2\2\2\17\13\3\2\2\2\20\3\3\2\2\2\21\22\7\5\2\2"+
		"\22\23\7\6\2\2\23\24\7\7\2\2\24\25\5\6\4\2\25\26\7\t\2\2\26\27\b\3\1\2"+
		"\27\37\3\2\2\2\30\31\7\5\2\2\31\32\7\6\2\2\32\33\5\6\4\2\33\34\7\t\2\2"+
		"\34\35\b\3\1\2\35\37\3\2\2\2\36\21\3\2\2\2\36\30\3\2\2\2\37\5\3\2\2\2"+
		" !\5\b\5\2!\"\5\6\4\2\"%\3\2\2\2#%\5\b\5\2$ \3\2\2\2$#\3\2\2\2%\7\3\2"+
		"\2\2&\'\7\5\2\2\'(\7\4\2\2()\7\b\2\2)*\7\4\2\2*\t\3\2\2\2\5\17\36$";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}