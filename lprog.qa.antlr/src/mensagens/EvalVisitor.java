/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensagens;

/**
 *
 * @author dnamorim
 */
public class EvalVisitor extends MensagensBaseVisitor<Double> {

	public String nrOrigem_maxSMS;
	public String maxSMS;
	private double custo_maxSMS = 0d;

	private String actualDate = null;

	@Override
	public Double visitSms(MensagensParser.SmsContext ctx) {
		if (!ctx.DATA().getText().equals(actualDate)) {
			actualDate = ctx.DATA().getText();
			System.out.printf("Data: %s\n", actualDate);
		}
		System.out.
			printf("%s ", (ctx.HORA() == null) ? "??:??" : ctx.
					   HORA().getText());
		double custoSMS = visit(ctx.destinos());
		if (custoSMS > custo_maxSMS) {
			nrOrigem_maxSMS = ctx.TELEFONE().getText();
			maxSMS = ctx.MENSAGEM().getText();
		}
		System.out.
			printf("%.3f\n", custoSMS);

		return custo_maxSMS;
	}

	@Override
	public Double visitDestinos(MensagensParser.DestinosContext ctx) {
		double somaCustos;

		somaCustos = visit(ctx.destino());

		if (ctx.destinos() != null) {
			somaCustos += visit(ctx.destinos());
		}

		return somaCustos;
	}

	@Override
	public Double visitDestino(MensagensParser.DestinoContext ctx) {
		System.out.printf("%s ", ctx.TELEFONE().getText());
		return Double.parseDouble(ctx.CUSTO().getText());
	}

}
