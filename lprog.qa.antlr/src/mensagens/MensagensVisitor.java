// Generated from Mensagens.g4 by ANTLR 4.5
package mensagens;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link MensagensParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface MensagensVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link MensagensParser#log}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLog(MensagensParser.LogContext ctx);
	/**
	 * Visit a parse tree produced by {@link MensagensParser#sms}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSms(MensagensParser.SmsContext ctx);
	/**
	 * Visit a parse tree produced by {@link MensagensParser#destinos}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDestinos(MensagensParser.DestinosContext ctx);
	/**
	 * Visit a parse tree produced by {@link MensagensParser#destino}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDestino(MensagensParser.DestinoContext ctx);
}