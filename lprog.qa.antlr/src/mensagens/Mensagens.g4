/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

grammar Mensagens;

log : sms /* NEWLINE */
	| sms NEWLINE log
    ;

sms : TELEFONE DATA HORA destinos MENSAGEM { if($MENSAGEM.getText().length() > 160) { notifyErrorListeners("Limite SMS excedido.\n"); }}
    | TELEFONE DATA destinos MENSAGEM { if($MENSAGEM.getText().length() > 160) { notifyErrorListeners("Limite SMS excedido.\n"); }}
    ;

/* destinos : destino+; */
destinos : destino destinos
         | destino
         ;

destino : TELEFONE HASH CUSTO HASH;

NEWLINE: [\n];
HASH: '#';
TELEFONE: (('+'|([0][0]))([0-9][0-9][0-9]?))?[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9];
DATA : (([0][1-9])|([1-2][0-9])|([3][0-1]))'/'(([0][1-9])|([1][0-2]))'/'[0-9][0-9][0-9][0-9];
HORA : (([0-1][0-9])|([2][0-3]))':'[0-5][0-9];
CUSTO : ([0]|([1-9][0-9]*))'.'[0-9][0-9][0-9];
MENSAGEM : '('[ a-zA-Z]+')';

WS : [ \t\r] -> skip;



