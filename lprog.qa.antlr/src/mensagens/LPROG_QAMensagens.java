/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mensagens;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

/**
 *
 * @author dnamorim
 */
public class LPROG_QAMensagens {

	/**
	 * @param args the command line arguments
	 */
	public static void main(String[] args) throws IOException {
		FileInputStream fis = new FileInputStream(new File("sms.txt"));
		MensagensLexer lexer = new MensagensLexer(new ANTLRInputStream(fis));
		CommonTokenStream tokens = new CommonTokenStream(lexer);
		MensagensParser parser = new MensagensParser(tokens);
		ParseTree tree = parser.log(); // parse

		EvalVisitor eval = new EvalVisitor();
		eval.visit(tree);
		System.out.
			printf("\nSMS com maior custo: %s (%s)", eval.nrOrigem_maxSMS, eval.maxSMS);
	}

}
