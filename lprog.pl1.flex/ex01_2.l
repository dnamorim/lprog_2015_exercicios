%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>
	#include <string.h>

	char *s;
	int n = 0;
%}

/*	Regras */
%%
[a-zA-Z0-9]+	{
					if (strcmp(yytext, s) == 0) {
						n++;
					}
				}
.
\n

%%

/* Rotinas em C */
int main (int argc, char **argv) {
	++argv; ++argc;

	if (argc > 0) {
		s = argv[0];
	} else {
		printf("\nPasse uma cadeia de caracteres por parâmetro.\n");
		return 1;
	}
	yylex();
	printf("Cadeias Detectadas = %d\n", n);
	return 0;
}