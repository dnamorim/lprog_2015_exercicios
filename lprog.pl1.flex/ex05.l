%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>
%}

/*	Regras */
%%
[0-9]+	{ printf("%s\n", yytext);	}
.+					
\n

%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}