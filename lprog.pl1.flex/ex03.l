%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>
%}

/*	Regras */
%%
[0-9][0-9]\-[A-Za-z][A-Za-z]\-[0-9][0-9]	{ printf("PT Valid License\n");}
[0-9][0-9]\-[0-9][0-9]\-[A-Za-z][A-Za-z]	{ printf("PT Valid License\n");}
[A-Za-z][A-Za-z]\-[0-9][0-9]\-[0-9][0-9]	{ printf("PT Valid License\n");}
.+	{printf("Invalid License\n");}
\n
%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}