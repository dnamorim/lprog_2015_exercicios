%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>

	int nAlgarismos = 0;
	int nLetAlfabeto = 0;
	int nLinhasTxt = 0;
	int nSpcTab = 0;
	int nOutros = 0;
%}

/*	Regras */
%%
[0-9]				{ nAlgarismos++;	}
[A-Za-z]			{ nLetAlfabeto++;	}
\n 					{ nLinhasTxt++;	}
[[:space:]]			{ nSpcTab++;	}
.					{ nOutros++;	}
%%

/* Rotinas em C */
int main (void) {
	yylex();
	printf("Número de Algarismos: %d\n", nAlgarismos);
	printf("Número de Letras do Alfabeto: %d\n", nLetAlfabeto);
	printf("Número de linhas de texto: %d\n", nLinhasTxt);
	printf("Número de espaços ou tabulações: %d\n", nSpcTab);
	printf("Número de Caracteres não identificados categorias anteriores: %d\n", nOutros);
	return 0;
}