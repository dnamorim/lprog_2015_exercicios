%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>
%}

/*	Regras */
%%
0|[+-]?[1-9][0-9]*	{ printf("Número Inteiro Válido.\n");	}
.+	{	printf("Número Inválido.\n"); }			
\n

%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}