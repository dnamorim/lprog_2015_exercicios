%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>
%}

/*	Regras */
%%
[fF][eE][uU][pP]	{
			printf("ISEP");
		}

2007	{
			printf("2008");
		}
.		{
			printf("%s", yytext);
		}
%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}
