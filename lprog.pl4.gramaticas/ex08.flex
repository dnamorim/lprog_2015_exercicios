%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>
%}

/*	Regras */
%%
\(						{	printf("Abre Parêntisis.\n");		}
\)						{	printf("Fecha Parêntisis.\n");		}
xor|or|and|not			{	printf("OPERADOR: %s\n", yytext);	}
[0-9]+\.[0-9]+			{	printf("REAL: %s\n", yytext); 		}
[0-9]+ 					{	printf("INT: %s\n", yytext);		}
[a-zA-Z_][a-zA-Z0-9]* 	{	printf("ID: %s\n", yytext);			}
[\n\t ]
.						{	printf("ERRO.\n");					}
%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}