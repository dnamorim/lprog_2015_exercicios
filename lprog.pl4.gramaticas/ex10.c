%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>

	enum {TOKEN_A, TOKEN_B, TOKEN_C, TOKEN_PO, TOKEN_PC, TOKEN_PLUS, OTHER, END};
	int token, n_erros = 0;

	void s();
	void a();
	void b();
	void c();

%}

/*	Regras */
%%
a|A 	return TOKEN_A;
b|B 	return TOKEN_B;
c|C 	return TOKEN_C;
d|D 	return TOKEN_D;
\(		return TOKEN_PO;
\)		return TOKEN_PC;
\+		return TOKEN_PLUS;
.		return OTHER;
\n 		return END;
<<EOF>>	return END;
%%

void error(char *s) {
	n_erros++;
	printf("%s <- Erro %d: %s\n", yytext, n_erros, s);
	exit(EXIT_FAILURE);
}

void getToken() {
	printf("%s", yytext);
	token = yylex();
}

/* Rotinas em C */
int main (void) {
	token = yylex(); /* vai buscar o 1º TOKEN */
	s(); /* Produção Inicial */

	if (n_erros == 0 && token == END) {
		printf("\nExpressão Válida!\n");
	} else {
		error("Símbolo não reconhecido (esperava a, b, c, (, ) ou +)");
	}

	return EXIT_SUCCESS;
}

/*	S -> AB */
void s() {
	a();
	b();
}

/*	A -> aB | e */
void a() {
	switch (token) {
		case TOKEN_A: getToken(); b(); break;
		case END: break;
		default:
			error("Símbolo não reconhecido (esperava a ou fim)");
	}
}

/*	B -> bAC | e */
void b() {
	switch (token) {
		case TOKEN_B: getToken(); a(); c(); break;
		case END: break;
		default:
			error("Símbolo não reconhecido (esperava b ou fim)");
	}
}

/* C -> c(A+B) */
void c() {
	switch (token) {
			case TOKEN_C: 
					getToken(); 
					if (token == TOKEN_PO) {
						getToken();
						a();
						getToken();
						if (token == TOKEN_PLUS) {
							getToken();
							b();
						}
						
					}
					break;
			case END: break;
			default:
				error("Símbolo não reconhecido (esperava a ou fim)");
	}
}


