%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>

%}

/*	Regras */
%%
#[A-Za-z]*	{	printf("Comentário\n");	}
(90|[1-8]?[0-9])º[0-5]?[0-9]’([0-5]?[0-9]”)?[NS]	{	printf("Latitude\n");	}
(180|1[0-7][0-9]|[0-9]?[0-9])º[0-5]?[0-9]’([0-5]?[0-9]”)?[WE]	{	printf("Longitude");	}
[A-Z][a-z]*	{	printf("Cidade\n");	}
([0-1]?[0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]	{	printf("Hora\n");	}
((3[01]|[12][0-9]|[1-9])-(0[13578]|10|12)-[12][0-9]{3})|((30|[12][0-9]|[1-9])-(0[469]|11)-[12][0-9]{3})|(([12][0-9]|[1-9])-02-[12][0-9]{3})	{	printf("Data");	}
.+
\n
%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}