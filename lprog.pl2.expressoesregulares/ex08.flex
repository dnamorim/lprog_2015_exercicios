%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>

%}

/*	Regras */
%%
(€0|€[1-9][0-9]*),[0-9]{2}|(0|[1-9][0-9]*),[0-9]{2}EUR	{	printf("Moeda - Euro\n"); }
(£0|£[1-9][0-9]*),[0-9]{2}	{	printf("Moeda - Libra\n");}
($0|$[1-9][0-9]*),[0-9]{2}	{	printf("Moeda - Dólar\n");}
(0|[1-9][0-9]*)$[0-9]{2}	{	printf("Moeda - Escudo\n");}
.+							{	printf("Moeda não reconhecida.\n");}
\n
%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}