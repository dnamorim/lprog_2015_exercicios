%option noyywrap
/*	Declarações */
%{
	#include <stdio.h>
%}

/*	Regras */
%%
[1-9][0-9]{3}(\-[0-9]{3})?' '[A-Z][ a-zA-Z]+ {	printf("Valid PT ZIP Code\n");}
.+	{	printf("Invalid ZIP Code\n");	}	
\n

%%

/* Rotinas em C */
int main (void) {
	yylex();
	return 0;
}