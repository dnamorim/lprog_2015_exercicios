%{
	#include <stdio.h>
	#include <stdlib.h>

	/* Declarar Variáveis C */
	int numErrors = 0;
	double memory[26];

	/* Declaração Funções C */
	int yyerror(char *str);
	int yylex (void);
%}

/* Definir Tokens sendo possíveis de guardar */
%union {
	char cval;
	int ival;
	double dval; 
}

%token <ival> TK_INT
%token <dval> TK_REAL
%token <cval> TK_ID
%token TK_EQ TK_SUM TK_SUB TK_MUL TK_DIV TK_PAO TK_PAC

%type <dval> value expr /* Valor de retorno irá usar dval */

%left TK_SUB TK_SUM
%left TK_MUL TK_DIV

%start expressions

%%
expressions	:	/* vazio */
			|	expressions formula '\n'
			|	expressions '\n'
			|	expressions error '\n' { yyerror("incorrect expression"); }
			;

/* Regra S -> ID = E | E */
formula		:	TK_ID TK_EQ expr 	{ memory[$1-'a'] = $3; printf("%c : %lf\n", $1, $3); }
			|	expr 				{ printf("%lf\n", $1); }
			;

/* Regra E -> E+E | E-E | E*E | E/E | -E | (E) | ID | INT | REAL */
expr 		:	expr TK_SUM expr { $$ = $1 + $3; }
			|	expr TK_SUB expr { $$ = $1 - $3; }
			|	expr TK_MUL expr { $$ = $1 * $3; }
			|	expr TK_DIV expr 	{ 	if ($3 == 0) {
											yyerror("division by 0");
											exit(-1);
										} else {
											$$ = $1 / $3;
										}
									}
			|	TK_SUB expr { $$ = -$2; }
			|	TK_PAO expr TK_PAC { $$ = $2; }
			|	value { $$ = $1; }
			;


value 	: TK_ID 	{ $$ = memory[$1 - 'a']; }
		| TK_INT	{ $$ = $1; }
		| TK_REAL	{ $$ = $1; }
		;
%%

int main(void) {
	yyparse();
	return 0;
}			


int yyerror(char *str) {
	numErrors++;
	printf("Syntax Error : %s\n", str);
	return numErrors;
}

