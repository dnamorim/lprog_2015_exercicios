%{ 
	#include <ctype.h>
	#include "ex05a.tab.h" /* Header gerado pelo Bison */

	extern int numErrors;
%} 

%% 
(0|[1-9][0-9]*)\.[0-9]+	{ 
								yylval.dval = atof(yytext); 
								return TK_REAL; 
							}

(0|[1-9][0-9]*)	{ 
					yylval.ival = atoi(yytext);  
					return TK_INT; 
				}

[a-zA-Z]		{ 
					yylval.cval = tolower(yytext[0]);
					return TK_ID; 
				}

\=				{ return TK_EQ; }
\+				{ return TK_SUM; }
\-				{ return TK_SUB; }
\*				{ return TK_MUL; }
\/				{ return TK_DIV; }
\(				{ return TK_PAO; }
\)				{ return TK_PAC; }
\n 				{ return yytext[0]; }
[\t\r ] 		/* ignorado */

.				{ puts("Lexical error: character not recognized"); numErrors++;} 

<<EOF>>         return 0; 

%%

