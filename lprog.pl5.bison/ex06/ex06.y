
%{
	#include <stdio.h>
	#include <stdlib.h>

	/* Declarar Variáveis C */
	int numErrors = 0;

	/* Declaração Funções C */
	int yyerror(char *str);
	int yylex (void);
%}

/* Definir Tokens sendo possíveis de guardar */
%union {
	double value; 
}

%token <value> TK_MOEDA TK_CAFE TK_PINGO TK_CHA TK_CHOCOLATE TK_COPO TK_LEITE

%type <value> moedas

%start orders

%%
orders	:	/* vazio */
			|	orders order '\n'
			|	orders '\n'
			|	orders error '\n' { yyerror("produto/moeda inválida"); }
			;

order	:	TK_CAFE ',' moedas		{ if($3 >= $1) { printf("Café - Troco: €%.2f\n", $3-$1); } else { printf("Café - dinheiro insuficiente\n"); } }
		|	TK_CHA ',' moedas		{ if($3 >= $1) { printf("Chá - Troco: €%.2f\n", $3-$1); } else { printf("Chá - dinheiro insuficiente\n"); } }
		|	TK_COPO ',' moedas		{ if($3 >= $1) { printf("Copo - Troco: €%.2f\n", $3-$1); } else { printf("Copo - dinheiro insuficiente\n"); } }
		|	TK_CHOCOLATE ',' moedas	{ if($3 >= $1) { printf("Chocolate - Troco: €%.2f\n", $3-$1); } else { printf("Chocolate - dinheiro insuficiente\n"); } }
		|	TK_LEITE ',' moedas		{ if($3 >= $1) { printf("Leite - Troco: €%.2f\n", $3-$1); } else { printf("Leite - dinheiro insuficiente\n"); } }
		|	TK_PINGO ',' moedas		{ if($3 >= $1) { printf("Pingo - Troco: €%.2f\n", $3-$1); } else { printf("Pingo - dinheiro insuficiente\n"); } }
		;

moedas	:	TK_MOEDA {	$$ = $1; }
		|	moedas ',' TK_MOEDA { $$ = $1 + $3; }
		;

%%

int main(void) {
	yyparse();
	return 0;
}			


int yyerror(char *str) {
	numErrors++;
	printf("Syntax Error : %s\n", str);
	return numErrors;
}

