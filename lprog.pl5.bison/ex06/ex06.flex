%{ 
	#include <ctype.h>
	#include "ex06.tab.h" /* Header gerado pelo Bison */

	extern int numErrors;
%} 

%% 
cafe	{	yylval.value = 0.35;	return TK_CAFE; }
pingo	{	yylval.value = 0.35;	return TK_CAFE; }
cha		{	yylval.value = 0.35;	return TK_CAFE; }
copo	{	yylval.value = 0.05;	return TK_COPO; }
leite	{	yylval.value = 0.30;	return TK_LEITE; }
chocolate	{	yylval.value = 0.40;	return TK_CHOCOLATE; }

E([12]\.00|0\.([125]0|0[125]))	{ 
									/* Passa Endereço do início do número (pos 0 contém o €) */
									yylval.value = atof(++yytext); 
									return TK_MOEDA; 
								}
[\n\,]	return yytext[0];

[\t\r ]* 		/* ignorado */

.				{ printf("Lexical error: character not recognized - %c\n", yytext[0]); numErrors++;} 

<<EOF>>         return 0; 

%%

