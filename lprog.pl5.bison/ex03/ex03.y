%{
	#include <stdio.h>
	#include "types.h"

	int numErrors = 0;
	
	int yyerror(char *str);
	int yylex (void);

	void compare(TValue v1, int op, TValue v2);
%}

%union {
	TValue val;
}

%token <val> TK_NUM TK_CHAR 
%token TK_EQUALS TK_NOTEQUALS TK_GREATER TK_LESS TK_GREATEREQUALS TK_LESSEQUALS
%type <val> expr value

%start exprs_line

%%
exprs_line	:	/* vazio */
			|	exprs_line expr '\n'
			|	exprs_line '\n'
			;

expr	:		value	{ $$ = $1; }
			|	expr TK_EQUALS value		{ compare($1, TK_EQUALS, $3); $$ = $3;	}
			|	expr TK_NOTEQUALS value		{ compare($1, TK_NOTEQUALS, $3); $$ = $3;	}
			|	expr TK_GREATER value		{ compare($1, TK_GREATER, $3); $$ = $3;	}
			|	expr TK_LESS value			{ compare($1, TK_LESS, $3); $$ = $3;	}
			|	expr TK_GREATEREQUALS value	{ compare($1, TK_GREATEREQUALS, $3); $$ = $3;	}
			|	expr TK_LESSEQUALS value	{ compare($1, TK_LESSEQUALS, $3); $$ = $3;	}
			|	error { yyerror("incorrect expression"); }
			;

value	: 	TK_NUM { $$ = $1; }
		|	TK_CHAR { $$ = $1; }
		;
%%

int main(void) {
	yyparse();
	return 0;
}			

void compare(TValue v1, int op, TValue v2) {
	if (v1.type != v2.type) {
		puts("incompatible types"); 
		return;
	}

	switch (op) {
		case TK_EQUALS: 		puts(((v1.value == v2.value) ? "true" : "false")); break;
		case TK_NOTEQUALS:		puts(((v1.value != v2.value) ? "true" : "false")); break;
		case TK_GREATER:		puts(((v1.value > v2.value) ? "true" : "false")); break;
		case TK_GREATEREQUALS: 	puts(((v1.value >= v2.value) ? "true" : "false")); break;
		case TK_LESS: 			puts(((v1.value < v2.value) ? "true" : "false")); break;
		case TK_LESSEQUALS: 	puts(((v1.value <= v2.value) ? "true" : "false")); break;
		default: 				break;
	}
}

int yyerror(char *str) {
	numErrors++;
	printf("Sintax Error : %s\n", str);
	return numErrors;
}

