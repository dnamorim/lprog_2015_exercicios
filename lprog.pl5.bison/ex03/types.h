typedef enum { INT, CHAR } EType;

typedef struct {
	int value;
	EType type;
} TValue;