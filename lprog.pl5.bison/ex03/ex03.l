%{ 
	#include <stdlib.h>
	#include "types.h"
	#include "ex03.tab.h" 
%} 

%% 
[-+]?(0|[1-9][0-9]*)	{ 
							yylval.val.value = atoi(yytext); 
							yylval.val.type = INT; 
							return TK_NUM; 
						}

[a-zA-Z]		{ 
					yylval.val.value = yytext[0];
					yylval.val.type = CHAR;
					return TK_CHAR; 
				}

\=				return TK_EQUALS;
\<\>			return TK_NOTEQUALS;
\>				return TK_GREATER;
\>\=			return TK_GREATEREQUALS;
\<				return TK_LESS;
\<\=			return TK_LESSEQUALS;
\n 				return yytext[0];
[\t\r ] 		/* ignorado */
.				{ puts("Lexical error");} 

<<EOF>>         return 0; 

%%

