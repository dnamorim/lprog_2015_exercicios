%{ 
	#include <stdio.h> 
	int numErrors = 0; 
%} 

%union { 
	char *pal; 
} 

%token <pal> TK_HELLO TK_WORLD TK_WORD 
%start phrase 

%% 
phrase	: 	/* vazio */ 
	| phrase TK_HELLO 
	| words TK_WORLD 
	| phrase TK_WORD 
	; 

words	:	/* vazio */ 
	| phrase TK_HELLO {printf("Hello World!!!\n");} 
	| words TK_WORLD 
	| phrase TK_WORD 
	;

%% 

int main() 
{ 
	yyparse(); 
} 


int yyerror(char *s) 
{ 
	numErrors++; 
	printf("erro sintatico/semantico : %s\n", s); 
}	 


