%{ 
	#include <stdlib.h>
	#include "ex02.tab.h" 
%} 

%% 
0|[1-9][0-9]*	{ yylval.nr = atoi(yytext); return TK_NR; }
\=				return TK_EQUALS;
\<\>			return TK_NOTEQUALS;
\>\=			return TK_GREATEREQUALS;
\<\=			return TK_LESSEQUALS;
\>				return TK_GREATER;
\<				return TK_LESS;
\n 				return yytext[0];
[\t\r ] 		/* ignorado */
.				{ puts("Erro Lexico");} 
<<EOF>>         return 0; 

%%

