%{
	#include <stdio.h>
	int numErrors = 0;
%}

%union {
	int nr;
}

%token <nr> TK_NR 
%token TK_EQUALS TK_NOTEQUALS TK_GREATER TK_LESS TK_GREATEREQUALS TK_LESSEQUALS

%start exprs

%%
exprs	:	/* vazio */
			|	exprs expr '\n'
			|	exprs '\n'
			;

expr	:		TK_NR TK_EQUALS TK_NR { puts((($1 == $3) ? "Verdadeiro" : "Falso")); }
			|	TK_NR TK_NOTEQUALS TK_NR { puts((($1 != $3) ? "Verdadeiro" : "Falso")); }
			|	TK_NR TK_GREATER TK_NR { puts((($1 > $3) ? "Verdadeiro" : "Falso")); }
			|	TK_NR TK_GREATEREQUALS TK_NR { puts((($1 >= $3) ? "Verdadeiro" : "Falso")); }
			|	TK_NR TK_LESS TK_NR { puts((($1 < $3) ? "Verdadeiro" : "Falso")); }
			|	TK_NR TK_LESSEQUALS TK_NR { puts((($1 <= $3) ? "Verdadeiro" : "Falso")); }
			|	error /*{ yyerror("erro de sintaxe"); }*/
			;
			
%%

int main(void) {
	yyparse();
	return 0;
}			

int yyerror(char *str) {
	numErrors++;
	printf("Erro Sintactico/Semantico: %s\n", str);
}

