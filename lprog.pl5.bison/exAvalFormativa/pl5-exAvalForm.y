%{
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>

	/* Declarar Variáveis C */
	int numErrors = 0;
	
	int max_alunos = 0;
	char nome_escola[6];
	char buffer_docentesInf[5000];
	

	/* Declaração Funções C */
	int yyerror(char *str);
	int yylex (void);
%}

/* Definir Tokens sendo possíveis de guardar */
%union {
	int number;
	char string[40]; 
}

%token <number> TK_NR
%token <string> TK_STRING TK_ESCOLA

%start lista

%%
lista		:	/* vazio */
			|	lista escola '\n' docentes '\n'
			|	lista '\n'
			|	lista error '\n' { yyerror("lista inválida"); }
			;

escola		: 	TK_ESCOLA TK_NR TK_STRING { 
												if ($2 <= 100) {
													yyerror("número de alunos inválido");
												} else {
													printf("Escola: %s\nNr.Alunos: %d\nEndereço: %s\n", $1, $2, $3);
													if ($2 > max_alunos) {
														max_alunos = $2;
														strcpy(nome_escola, $1);
													}
												}
											}
			;

docentes	:	docente
			|	docentes '\n' docente
			;

docente 	: 	TK_STRING TK_NR TK_STRING 	{
												if ($2 <= 999) {
													yyerror("extensão inválida");
												} else {
													printf("Docente: %s\nExtensão: %d\nDepartamento: %s\n", $1, $2, $3);
													if (strcmp(nome_escola, "ISEP") == 0) {
														if (strcmp($3, "\"Engª Informática\"") == 0) {
															sprintf(buffer_docentesInf, "%s- Docente %s - Ext. %d\n", buffer_docentesInf, $1, $2);
														}
													}
												}
										 	}
			;
%%

int main(void) {
	yyparse();
	printf("\nEscola com Mais Alunos: %s (Nr. Alunos: %d)\n", nome_escola, max_alunos);
	printf("\nDocentes de Engª Informática do ISEP:\n%s\n", buffer_docentesInf);
	return 0;
}			


int yyerror(char *str) {
	numErrors++;
	printf("Syntax Error : %s\n", str);
	return numErrors;
}

