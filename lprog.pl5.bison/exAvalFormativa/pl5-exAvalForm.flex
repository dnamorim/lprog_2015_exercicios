%{ 
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>

	#include "pl5-exAvalForm.tab.h" /* Header gerado pelo Bison */

	extern int numErrors;
%} 

%option noyywrap

%% 
ISEP|ISCAP|ESE												{ strcpy(yylval.string, yytext); return TK_ESCOLA; }

1(0[1-9]|[1-9][0-9])|[2-9][0-9]{2}|[1-9][0-9]{3,}				{ yylval.number = atoi(yytext); return TK_NR;}

\"[^\"]{0,40}\"						{ strcpy(yylval.string, yytext); return TK_STRING; }

[\<\>\n]							{ return yytext[0];}

[ \t\r] 		/* ignorado */

.										{ printf("Lexical error: character not recognized - %c\n", yytext[0]); numErrors++;} 

<<EOF>>         return 0; 

%%

