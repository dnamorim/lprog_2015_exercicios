%{
	#include <stdio.h>
	#include <stdlib.h>

	/* Declarar Variáveis C */
	int numErrors = 0;
	double memory[26];

	/* Declaração Funções C */
	int yyerror(char *str);
	int yylex (void);
%}

/* Definir Tokens sendo possíveis de guardar */
%union {
	char cval;
	int ival;
	double dval; 
}

%token <ival> TK_INT
%token <dval> TK_REAL
%token <cval> TK_ID
%token TK_EQ TK_SUM TK_SUB TK_MUL TK_DIV TK_PAO TK_PAC

%type <dval> valor expr_pa expr_pb

%start expressions

%%
expressions	:	/* vazio */
			|	expressions formula '\n'
			|	expressions '\n'
			|	expressions error '\n' { yyerror("incorrect expression"); }
			;

formula		:	TK_ID TK_EQ expr_pb { memory[$1-'a'] = $3; printf("%c : %lf\n", $1, $3); }
			|	expr_pb 			{ printf("%lf\n", $1); }
			;

expr_pb		:	expr_pb TK_SUM expr_pa { $$ = $1 + $3; }
			| 	expr_pb TK_SUB expr_pa { $$ = $1 - $3; }
			| 	TK_SUB expr_pa { $$ = -$2; }
			| 	expr_pa { $$ = $1; }
			;

expr_pa		:	expr_pa TK_MUL valor { $$ = $1 * $3; }
			| 	expr_pa TK_DIV valor {	if ($3 == 0) {
											yyerror("division by 0");
											exit(-1);
										} else {
											$$ = $1 / $3;
										}
									 }

			| 	TK_PAO expr_pb TK_PAC { $$ = $2; }
			|	valor	{ $$ = $1; }
			;

valor	:	TK_ID 	{ $$ = memory[$1 - 'a']; }
		| 	TK_INT 	{ $$ = $1; }
		|	TK_REAL	{ $$ = $1; }
		;
%%

int main(void) {
	yyparse();
	return 0;
}			


int yyerror(char *str) {
	numErrors++;
	printf("Syntax Error : %s\n", str);
	return numErrors;
}

