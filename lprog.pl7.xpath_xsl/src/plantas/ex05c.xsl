<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : ex05c.xsl
	Created on : 15 de Maio de 2015, 17:12
	Author     : dnamorim
	Description:
		Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>

	<!-- TODO customize transformation rules
		 syntax recommendation http://www.w3.org/TR/xslt
	-->
	<xsl:template match="/">
		<html>
			<head>
				<title>LPROG - PL7: XSLT e XPATH - Exercício 5c)</title>
			</head>
			<body>
				<center>
					<h3>LPROG - PL7: XSLT e XPATH - Exercício 5c)</h3>
					<div>
						<h2>LISTAGEM DE PLANTAS</h2>

						<table border="1">
							<tr bgcolor="#d3d3d3">
								<th></th>
								<th>Nome</th>
								<th>Preço</th>
								<th>Disponibildiade</th>
							</tr>
							<xsl:apply-templates select="Catalogo/Planta"/>
						</table>
					</div>
				</center>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="Planta">
		<tr>
			<td>
				<xsl:value-of select="position()"/>
			</td>
			<td>
				<xsl:value-of select="NomeComum" />
			</td>
			<td>
				<center>
					<xsl:value-of select="Preco"/>
				</center>
			</td>
			<td>
				<center>
					<xsl:value-of select="Disponibilidade"/>
				</center>
			</td>
		</tr>
	</xsl:template>

</xsl:stylesheet>
