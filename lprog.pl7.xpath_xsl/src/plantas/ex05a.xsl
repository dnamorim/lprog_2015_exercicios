<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : plantas.xsl
	Created on : 14 de Maio de 2015, 15:59
	Author     : dnamorim
	Description:
		Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>

	<!-- TODO customize transformation rules
		 syntax recommendation http://www.w3.org/TR/xslt
	-->
	<xsl:template match="/">
		<html>
			<head>
				<title>LPROG - PL7: XSLT e XPATH - Exercício 5a)</title>
			</head>
			<body>
				<h2> LPROG - PL7: XSLT e XPATH - Exercício 5a) </h2>
				<p>
					<div>
						<b>1. Obter o nome comum da segunda planta: </b>
						<xsl:value-of select="Catalogo/Planta[2]/NomeComum" />
						<br/>
					</div>
					<div>
						<b>2. Obter a quantidade de plantas no documento: </b>
						<xsl:value-of select="count(/Catalogo/Planta)" />
						<br/>
					</div>
					<div>
						<b>3. Obter o preço da última planta da lista: </b>
						<xsl:value-of select="Catalogo/Planta[last()]/Preco" /> €
						<br/>
					</div>
					<div>
						<b>4. A média dos preços das plantas: </b>
						<xsl:value-of select="sum(Catalogo/Planta/Preco) div count(/Catalogo/Planta)" /> €
						<br/>
					</div>
					<div>
						<b>5. Formatar a média para duas casas decimais: </b>
						<xsl:value-of select="format-number(sum(Catalogo/Planta/Preco) div count(/Catalogo/Planta), '0.00')" /> €
						<br/>
					</div>
				</p>

			</body>
		</html>
	</xsl:template>

</xsl:stylesheet>
