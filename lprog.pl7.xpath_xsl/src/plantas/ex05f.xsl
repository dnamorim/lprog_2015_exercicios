<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : ex05f.xml.xsl
	Created on : 15 de Maio de 2015, 17:51
	Author     : dnamorim
	Description:
		Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>
	<xsl:key name="zonas" match="Planta" use="Zona"/>

	<!-- TODO customize transformation rules
		 syntax recommendation http://www.w3.org/TR/xslt
	-->
	<xsl:template match="/">
		<html>
			<head>
				<title>LPROG - PL7: XSLT e XPATH - Exercício 5f)</title>
			</head>
			<body>
				<center>
					<h2>LPROG - PL7: XSLT e XPATH - Exercício 5f)</h2>
					<table border="1" align="center">
						<tr bgcolor="#d3d3d3">
							<th>Zona</th>
							<th>Total Plantas da Zona</th>
						</tr>
						<xsl:apply-templates select="Catalogo">
							<xsl:sort select="Planta/Zona" />
						</xsl:apply-templates>
					</table>
				</center>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="Catalogo">
		<p>Nº de Zonas Distintas: <xsl:value-of select="count(Planta[generate-id() = generate-id(key('zonas',Zona)[1])])"/></p>
		<xsl:for-each select="Planta[generate-id() = generate-id(key('zonas',Zona)[1])]">
			<xsl:sort select="Zona"/>
			<tr>
				<td align="center">
					<xsl:value-of select="Zona"/>
				</td>
				<td align="center">
					<xsl:value-of select="count(/Catalogo/Planta[Zona=current()/Zona])"/>
				</td>
			</tr>
		</xsl:for-each>
	</xsl:template>
</xsl:stylesheet>