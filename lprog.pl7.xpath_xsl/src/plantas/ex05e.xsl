<?xml version="1.0" encoding="UTF-8"?>

<!--
	Document   : ex05c.xsl
	Created on : 15 de Maio de 2015, 17:12
	Author     : dnamorim
	Description:
		Purpose of transformation follows.
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
	<xsl:output method="html"/>

	<!-- TODO customize transformation rules
		 syntax recommendation http://www.w3.org/TR/xslt
	-->
	<xsl:template match="/">
		<html>
			<head>
				<title>LPROG - PL7: XSLT e XPATH - Exercício 5e)</title>
			</head>
			<body>
				<center>
					<h3>LPROG - PL7: XSLT e XPATH - Exercício 5e)</h3>
					<div>
						<h2>LISTAGEM DE PLANTAS</h2>

						<table border="1">
							<tr bgcolor="#d3d3d3">
								<th></th>
								<th>Nome</th>
								<th>Preço</th>
								<th>Disponibildiade</th>
								<th>Luz</th>
							</tr>
							<xsl:apply-templates select="Catalogo/Planta[Preco &lt; 4]"/>
						</table>
					</div>
				</center>
			</body>
		</html>
	</xsl:template>

	<xsl:template match="Planta">
		<tr>
			<td>
				<xsl:value-of select="position()"/>
			</td>
			<td>
				<xsl:value-of select="NomeComum" />
			</td>
			<td>
				<center>
					<xsl:value-of select="Preco"/>
				</center>
			</td>
			<td>
				<center>
					<xsl:value-of select="Disponibilidade" />
				</center>
			</td>
			<xsl:choose>
				<xsl:when test="@NecessidadeLuz = 'Sombra'">
					<td>
						<img src="sources/cloudy.png" style="width:30px;height:30px" />
					</td>
				</xsl:when>
				<xsl:when test="@NecessidadeLuz = 'Preferencialmente Sombra'">
					<td>
						<img src="sources/mostly_cloudy.png" style="width:30px;height:30px" />

					</td>
				</xsl:when>
				<xsl:when test="@NecessidadeLuz = 'Sol'">
					<td>
						<img src="sources/sunny.png" style="width:30px;height:30px" />
					</td>
				</xsl:when>
				<xsl:when test="@NecessidadeLuz = 'Sol ou Sombra' or @NecessidadeLuz = 'Preferencialmente Sol'">
					<td>
						<img src="sources/partly_sunny.png" style="width:30px;height:30px" />
					</td>
				</xsl:when>

				<xsl:otherwise>
					<td>-</td>
				</xsl:otherwise>
			</xsl:choose>
		</tr>
	</xsl:template>

</xsl:stylesheet>
